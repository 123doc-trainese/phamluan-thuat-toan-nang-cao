<?php
include_once "MyLinklist.php";
$linkList = new MyLinklist();
while (true) {
    echo "Lua chon: \n";
    echo "1. Them node moi vao dau\n";
    echo "2. Them node moi vao cuoi \n";
    echo "3. Them node moi tai vi tri index \n";
    echo "4. Lay gia tri tai vi tri bat ki\n";
    echo "5. xoa node\n";
    echo "6. in ra danh sach\n";
    echo "exit: exit program\n";
    $line = readline();
    if ($line == "exit") {
        break;
    }

    switch ($line) {
        case "1":
            echo "Nhap gia tri cho node: ";
            $value = readline();
            $linkList->addAtHead($value);
            $linkList->printList();
            break;
        case "2":
            echo "Nhap gia tri cho node: ";
            $value = readline();
            $linkList->addAtTail($value);
            $linkList->printList();
            break;
        case "3":
            echo "Nhap vi tri: ";
            $index = readline();
            echo "Nhap gia tri ";
            $value = readline();
            $linkList->addAtIndex($index, $value);
            $linkList->printList();
            break;
        case "4":
            echo "Nhap vi tri: ";
            $index = readline();
            $value = $linkList->get($index);
            if ($value == -1) {
                echo "Vi tri khong hop le \n";
            } else {
                echo "Gia tri: $value\n";
            }
            $linkList->printList();
            break;
        case "5":
            echo "Nhap vi tri: ";
            $index = readline();
            $linkList->deleteAtIndex($index);
            $linkList->printList();
            break;
        case "6":
            $linkList->printList();
            break;
    }
}
