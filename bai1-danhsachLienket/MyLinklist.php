<?php
include_once "Node.php";

class MyLinklist
{
    private $head;

    /**
     * @param int $index
     * @return int
     * @todo lay node tai vi tri index
     */
    public function get($index)
    {
        $length = $this->totalNode();
        if ($index <= 0 || $index > $length) {
            return -1;
        } else {
            $current = $this->head;
            for ($i = 1; $i < $index; $i++) {
                $current = $current->next;
            }
            return $current->value;
        }
    }

    /**
     * @todo them mot node moi vao dau danh sach
     * @param int $value
     */
    public function addAtHead($value)
    {
        $node = new Node($value);
        if ($this->head == null) {
            $this->head = $node;
        } else {
            $node->next = $this->head; //cho node moi tro den node dau tien
            $this->head = $node; //cho con tro head tro den node moi
        }
    }

    /**
     * @todo them mot node moi vao cuoi danh sach
     * @param int $value
     */
    public function addAtTail($value)
    {
        $node = new Node($value);
        if ($this->head == null) {
            $this->head = $node;
        } else {
            $last = $this->head;
            while ($last->next != null) {   //di chuyen den phan tu cuoi cung
                $last = $last->next;
            }
            $last->next = $node;
        }
    }

    /**
     * @param int $index
     * @param int $value
     * @todo Them mot nut co gia tri $value truoc nut $index
     */
    public function addAtIndex($index, $value)
    {
        $length = $this->totalNode();
        if ($index > $length || $index <= 0) {
            return;
        } elseif ($index == $length) {
            $this->addAtTail($value);
        } elseif ($index == 1) {
            $this->addAtHead($value);
        } else {
            $current = $this->head;
            for ($i = 1; $i < $index - 1; $i++) {   //di chuyen den vi tri index-1
                $current = $current->next;
            }
            $node = new Node($value);
            $tmp = $current->next;
            $current->next = $node;                 //node tai index-1 tro den node moi
            $node->next = $tmp;                     //node moi tro den index+1
        }
    }

    /**
     * @param int $index
     * @todo xoa nut co vi tri $index
     */
    public function deleteAtIndex($index)
    {
        $length = $this->totalNode();
        if ($index <= 0 || $index > $length) {
            return;
        } elseif ($index == 1) {
            $this->head = $this->head->next;
        } elseif ($index == $length) {
            $current = $this->head;
            for ($i = 1; $i < $length - 1; $i++) {
                $current = $current->next;
            }
            $current->next = null;
        } else {
            $current = $this->head;
            for ($i = 1; $i < $index - 1; $i++) {   //di chuyen den vi tri index-1
                $current = $current->next;          
            }
            $tmp = $current->next;                  //tmp chinh la node can xoa
            $current->next = $tmp->next;            //tro con tro hien tai den vi tri tiep theo (bo qua tmp)
        }
    }

    /**
     * @todo dem so luong node trong danh sach
     */
    public function totalNode()
    {
        $count = 0;
        $current = $this->head;
        while ($current != null) {
            $count++;
            $current = $current->next;
        }
        return $count;
    }

    /**
     * @todo In ra danh sach lien ket
     */
    public function printList()
    {
        $current = $this->head;
        echo "danh sach hien tai: ";
        while ($current != null) {
            echo $current->value . "->";
            $current = $current->next;
        }
        echo "\n";
    }
}
