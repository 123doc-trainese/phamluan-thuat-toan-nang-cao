# PhamLuan thuật toán nâng cao

***Thuật toán nâng cao***
- Tạo danh sách liên kết 
- Extract keyword từ văn bản
- Tóm tắt văn bản

Thực hiện bởi [Luận Phạm](https://facebook.com/phluann)

***Tham khảo***

### Bài 1: Tạo danh sách liên kết
- thiết kế việc triển khai danh sách liên kết (sử dụng danh sách liên kết đơn hoặc kép)
- Một nút trong danh sách được liên kết đơn lẻ phải có hai thuộc tính: val và next. val là giá trị của nút hiện tại và next là một con trỏ / tham chiếu đến nút tiếp theo.
- Nếu bạn muốn sử dụng danh sách liên kết kép, bạn sẽ cần thêm một thuộc tính trước để chỉ ra nút trước đó trong danh sách được liên kết
- Xây dựng class MyLinkedList:
    - get (int $index): int : Nhận giá trị của nút chỉ mục trong danh sách liên kết. Nếu chỉ mục không hợp lệ, hãy trả về -1.
    - addAtHead(int val): void: Thêm một nút có giá trị val trước phần tử đầu tiên của danh sách liên kết. Sau khi chèn, nút mới sẽ là nút đầu tiên của danh sách liên kết.
    - addAtTail(int val): void: Nối một nút có giá trị val làm phần tử cuối cùng của danh sách được liên kết.
    - addAtIndex(int index, int val): void: Thêm một nút có giá trị val trước nút index trong danh sách liên kết. Nếu index bằng độ dài của danh sách liên kết, nút sẽ được nối vào cuối danh sách liên kết. Nếu index lớn hơn độ dài, nút sẽ không được chèn.
    - deleteAtIndex(int index):void: Xóa nút có vị trí index trong danh sách liên kết, nếu chỉ mục hợp lệ.
    - deleteAtIndex(int index):void: Xóa nút có vị trí index trong danh sách liên kết, nếu chỉ mục hợp lệ.
    - print(): void: In ra danh sách liên kết

### Bài 2: Extract keyword từ văn bản
- sử dụng thuật toán rake extract keyword từ văn bản [text.txt](https://trello.com/1/cards/630880ef8b5e70010e975c87/attachments/63088a97c51e730197fbd814/download/text.txt)
- sử dụng bộ [stopwords](https://trello.com/1/cards/630880ef8b5e70010e975c87/attachments/63088a96dab43a015a863f37/download/stopword.json)

***Rake***
- Rake là một phương pháp trích xuất từ khóa được đề xuất vào năm 2010
- Bước 1: tách đoạn văn thành các câu đơn, loại bỏ các stopwords khỏi văn bản
- Bước 2: liệt kê các ứng viên
    - chúng ta sẽ tách văn bản dựa trên vị trí của các stopword và dấu câu, thu được các từ và cụm từ 
    - các từ và cụm từ đứng cạnh nhau không bị chia cách bởi các stopword hoặc các dấu câu sẽ được coi là các ứng viên
- Bước 3: chấm điểm ứng viên:
    - đầu tiên, đếm tần xuất xuất hiện của các từ đơn trong danh sách các từ khóa ứng viên (freq(w))
    - Tiếp theo, các từ đồng xuất hiện cũng sẽ được thống kê và bậc (degree) cho mỗi từ là tổng của chúng (deg(w))
    - tiếp theo thực hiện chia deg(w) cho freq(w) để có được điểm số cho các từ đơn. Việc này giúp ta thấy được từ nào xuất hiện nhiều hơn trong cụm so với đứng riêng lẻ
- Bước 4: xếp hạng cuối cùng
    - tính điểm của các từ khóa ứng viên bằng tổng điểm của các thành phần của nó
    - sau đó top N các ứng viên có điểm số cao sẽ được coi là các từ khóa của bài viết
- tham khảo: 
    - [Blog luyencode.net: trích rút từ khóa tự động](https://blog.luyencode.net/trich-rut-tu-khoa-tu-dong-voi-hoc-khong-giam-sat/#rapid-automatic-keyword-extraction-rake)
    - [Analytics vidhya: Rapid Keyword Extraction (RAKE) Algorithm](https://www.analyticsvidhya.com/blog/2021/10/rapid-keyword-extraction-rake-algorithm-in-natural-language-processing/)

### Bài 3: Tóm tắt văn bản
- sử dụng thuật toán textRank để tóm tắt văn bản bằng cách nối các câu có điểm cao nhất

***textRank***
- textRank là một kĩ thuật tóm tắt văn bản không giám sát
- ý tưởng:
    - chia văn bản thành các câu riêng lẻ
    - loại bỏ stopword khỏi các câu để nhận được các từ khóa
    - tạo ma trận từ của câu
    - tạo ma trận liên kết cho các từ (từ này liên kết đến từ khác)
    - tính điểm cho từng từ dựa vào số liên kết, từ đó tính được điểm cho từng câu
    - chọn những câu có điểm cao nhất để tạo thành văn bản tóm tắt