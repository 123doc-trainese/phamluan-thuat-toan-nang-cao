<?php
    namespace textRank;
    require 'vendor/autoload.php';
    use Lib\Parser;
    use Lib\Graph;
    use Lib\Score;
    use Lib\StopWord;
    use Lib\Summarize;

    class TextRank{
        private $stopWords = [];
        private $content;

        public function __construct($content)
        {
            $this->content = $content;
            $this->stopWords = new StopWord(__DIR__.'/../stopword.json');
        }
        
        /**
         * Lay ra cac cau co diem cao nhat
         * @param string $content nội dung cần rút gọn
         * @param int $max số câu có điểm cao nhất cần lấy ra
         */
        public function getSentences(string $content, int $max){
            $maxSentences = $max;           //số câu trong kết quả
            $parse = new Parser();          
            $parse->setContent($content);   
            $texts = $parse->parse();       // lấy ra các câu trong đoạn văn, ma trận từ của 1 câu, điểm của câu

            $graph = new Graph();
            $graph->createGraph($texts);    // lấy ra ma trận từ và vị trí liên kết của từ trong câu

            $score = new Score();
            $scores = $score->score($graph, $texts);    //tính điểm cho các từ trong câu

            $summarize = new Summarize();
            return $summarize->getSummarize($scores, $graph, $texts, $maxSentences);    //// trả về mảng các câu tóm tắt
        }
    }
?>