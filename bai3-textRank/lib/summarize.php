<?php
    namespace Lib;
    class Summarize{
        private $sentenceWeight = [];

        /**
         * Lấy ra đoạn văn tóm tắt
         */
        public function getSummarize(array $scores, Graph $graph, Text $text, int $sentenceLimit){
            $graphData = $graph->getGraph();
            $sentences = $text->getSentences();
            $this->findAndWeightSentences($scores, $graphData);                     // tính điểm cho từng câu
            return $this->getAllImportant($sentences, $sentenceLimit);              //lấy các câu có điểm cao   
        }
        
        /**
         * tìm và điền điểm cho từng câu
         */
        protected function findAndWeightSentences(array $scores, array $graphData){
            $i = 0;
    
            foreach ($scores as $word => $score) {              // duyệt từ và điểm từng từ
                $i++;
                $wordMap = $graphData[$word];                   // lấy từ ma trận connection lấy từng từ
                foreach ($wordMap as $key => $value) {
                    $this->updateSentenceWeight($key);          // tính điểm cho câu chứa từ
                }
            }
            arsort($this->sentenceWeight);                      // sắp xếp câu theo điểm
        }

        /**
         * lấy tất cả các câu quan trọng
         */
        protected function getAllImportant(array $sentences, int $sentenceLimit){
            $summary = [];
            $i = 1;
            foreach ($this->sentenceWeight as $sentenceIdx => $weight) {
                if ($i > $sentenceLimit) {                         // vượt quá sô câu thì thoát
                    break;
                }
                $i++;
                $summary[$sentenceIdx] = $sentences[$sentenceIdx]; // đưa ra câu kèm vị trí câu trong đoạn văn
            }
            ksort($summary); // sắp xếp lại câu
            return $summary;
        }

        /**
         * cập nhật điểm cho câu
         */
        protected function updateSentenceWeight(int $sentenceIdx){
            if (isset($this->sentenceWeight[$sentenceIdx])) {
                $this->sentenceWeight[$sentenceIdx] = $this->sentenceWeight[$sentenceIdx] + 1;
            } else {
                $this->sentenceWeight[$sentenceIdx] = 1;
            }
        }
    }
?>