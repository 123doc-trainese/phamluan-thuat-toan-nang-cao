<?php
    namespace Lib;
    class Text{
        /**
         * Mảng đa chiều chứa các từ khóa của đoạn văn, khóa là vị trí của câu
         * giá trị là một mảng chứa các từ của câu, khóa là vị trí của từ trong câu, giá trị là từ đó
         */
        private $wordMatrix = [];
        /**
         * Mảng chứa các câu trong đoạn văn, khóa là vị trí của câu, giá trị là câu đó
         */
        private $sentences = [];
        public function setWordMatrix(array $wordMatrix)
        {
            $this->wordMatrix = $wordMatrix;
        }

        public function setSentences(array $sentences){
            $this->sentences = $sentences;
        }

        public function getWordMatrix(){
            return $this->wordMatrix;
        }

        public function getSentences(){
            return $this->sentences;
        }
    }
?>