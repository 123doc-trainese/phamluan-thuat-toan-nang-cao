<?php
    namespace Lib;
    class Graph{
        private $graph = [];
    
        public function getGraph(){
            return $this->graph;
        }

        public function createGraph(Text $text){                //xây dựng từ và liên kết từng từ của câu
            $wordMatrix = $text->getWordMatrix();               // lấy các từ trong 1 câu ( đã cắt từ khóa)
            foreach ($wordMatrix as $sentenceIdx => $words) {   // duyệt qua các từ trong câu
                $idxArray = array_keys($words);                 // lấy vị trí các từ trong câu

                foreach ($idxArray as $idxKey => $idxValue) {   // duyệt mảng vị trí các từ
                    $connections = [];                          // lấy từ nối của các từ

                    if (isset($idxArray[$idxKey - 1])) {        // nếu có từ liền trước thì lấy liên kết là từ liên trước
                        $connections[] = $idxArray[$idxKey - 1];
                    }

                    if (isset($idxArray[$idxKey + 1])) {        // mếu có từ liền sau thì lấy liên kết là từ liền sau
                        $connections[] = $idxArray[$idxKey + 1];
                    }

                    $this->graph[$words[$idxValue]][$sentenceIdx][$idxValue] = $connections; // trả về ma trận [từ]['id câu']['vị trí từ] = vị trí các từ nối
                }
            }
        }
    }
?>