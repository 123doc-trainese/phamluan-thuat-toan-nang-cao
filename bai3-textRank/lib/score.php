<?php
    namespace Lib;
    class Score{
        private $maxValue = 0;
        private $minValue = 0;

        /**
         * tính điểm cho từ dựa vào liên kết ( điểm từ 0 -> 1)
         */
        public function score(Graph $graph, Text $text)
        {
            $graphData = $graph->getGraph();                        // lấy ra ma trận từ và connections
            $wordMatrix = $text->getWordMatrix();                   // lấy ma trận từ trong 1 câu
            $wordConnections = $this->getConnection($graphData);    // tính số lượng connections của từ
            $scores = $this->calculateScores(
                $graphData,
                $wordMatrix,
                $wordConnections
            );

            return $this->sortScores($scores);
        }

        /**
         * tính số lượng liên kết của từ trong câu
         */
        public function getConnection(array $graphData){
            $wordConnections = [];
            foreach ($graphData as $wordKey => $sentences) {
                $connectionCount = 0;
                foreach ($sentences as $sentenceIdx => $wordInstances) { // duyệt ma trận liên kết của từng từ
                    foreach ($wordInstances as $connections) {
                        $connectionCount += count($connections);        //đếm số liên kết của từng từ
                    }
                }
                $wordConnections[$wordKey] = $connectionCount;          // mảng liên kết với khóa là từ, giá trị là số liên kết
            }
            return $wordConnections;
        }

        /**
         * tính số liên kết cho từng từ trong bài
         */
        public function calculateScores(
            array $graphData,                                           // ma trận liên kết
            array $wordMatrix,                                          // ma trận từ của 1 câu
            array $wordConnections                                      // số liên kết của từ
        ){
            $scores = [];
            foreach ($graphData as $wordKey => $sentences) {            // duyệt ma trận connection của từng từ
                $value = 0;
                foreach ($sentences as $sentenceIdx => $wordInstances) {
                    foreach ($wordInstances as $connections) {
                        foreach ($connections as $wordIdx) {            // duyệt connections của từng từ
                            $word = $wordMatrix[$sentenceIdx][$wordIdx]; // lấy từ của câu trong ma trận từ ($centenceIdx => id của câu, $wordIdx => id của từ trong câu $centenceIdx) 
                            $value += $wordConnections[$word];          //tổng connections của từ trong bài
                        }
                    }
                }
                $scores[$wordKey] = $value;                             // mảng gồm từ và tổng số kết nối của từ trong đoạn văn
                if ($value > $this->maxValue) {                         // tìm số kết nối nhiều nhất
                    $this->maxValue = $value;
                }
                if ($value < $this->minValue || $this->minValue == 0) {
                    $this->minValue = $value;                           // số kết nối it nhất
                }
            }
            return $scores;
        }

        /**
         * tính lại điểm (điểm từ 0-1) và sắp xếp
         */
        public function sortScores(array $scores){
            foreach ($scores as $key => $value) {
                $v = $this->normalize($value, $this->minValue, $this->maxValue);
                $scores[$key] = $v;         // điểm cho từng từ
            }
            arsort($scores);                // sắp xếp mảng theo điểm(sắp xếp theo value)
            return $scores;
        }

        private function normalize(int $value, int $min, int $max){
            $divisor = $max - $min;         // tìm số chia (số connections max - connections min)
            if ($divisor == 0) {            // nếu sô chia = 0 trả về 0
                return 0.0;
            }
            $normalized = ($value - $min) / $divisor; // số connections của từ - min / sô chia
            return $normalized;
        }
    }
?>