<?php
    namespace Lib;

    class Parser{
        protected $minimumWordLength = 0;
        protected $content = '';
        protected $marks = [];
        protected $stopWords;

        /**
         * Lấy nội dung đoạn văn
         */
        public function setContent(string $content) 
        {
            $this->content = $content;
        }

        /**
         * tách văn bản
         */
        public function parse()
        {
            $matrix = [];
            $sentences = $this->getSentences(); // danh sách các câu văn

            foreach ($sentences as $sentenceIdx => $sentence) {     //duyệt qua các câu
                $matrix[$sentenceIdx] = $this->getWords($sentence); // lấy các từ trong câu
            }

            $text = new Text();
            $text->setSentences($sentences);    // các câu
            $text->setWordMatrix($matrix);      // ma trận từ trong từng câu
            return $text;
        }

        /**
         * Chia đoạn văn thành các câu riêng biệt
         */
        protected function getSentences()
        {
            $sentences = $sentences = preg_split('/(\n+)|(\\.\s)|(\\?\s)|(\\!\s)/', $this->content);    // lấy các câu trong đoạn văn

            return array_values(array_filter(array_map([$this, 'cleanSentence'], $sentences)));         // lấy các câu văn đã clean (loại bỏ các câu không có ký tự)
        }

        /**
         * tách câu thành các từ khóa
         */
        protected function getWords(string $subText)
        {
            $words = preg_split(
                '/(?:(^\p{P}+)|(\p{P}*\s+\p{P}*)|(\p{P}+$))/',
                $subText,
                -1,
                PREG_SPLIT_NO_EMPTY     //chuỗi trống được xóa khỏi mảng kết quả
            );
            $words = array_values(      //Loại bỏ các khoảng trắng thừa, chuyển từ về chữ thường
                array_filter(
                    array_map(
                        [$this, 'cleanWord'],
                        $words
                    )
                )
            );

            if ($this->stopWords) {
                return array_filter($words, function ($word) {          
                    return strlen($word) > $this->minimumWordLength     //độ dài lớn hơn độ dài nhỏ nhất, không nằm trong số stopword thì được trả về
                        && !$this->stopWords->exist($word);
                });
            } else {
                return array_filter($words, function ($word) {
                    return strlen($word) > $this->minimumWordLength;
                });
            }
        }

        /**
         * Loại bỏ các khoảng trống không cần thiết trong câu
         */
        protected function cleanSentence(string $sentence)
        {
            return trim($sentence);
        }

        /**
         * Loại bỏ các khoảng trắng thừa, chuyển từ về chữ thường
         */
        protected function cleanWord(string $word)
        {
            return mb_strtolower(trim($word));
        }
    }
?>