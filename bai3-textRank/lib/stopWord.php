<?php
    namespace Lib;
    class StopWord{
        protected $words = array(); // mảng từ khóa

        public function __construct($path)
        {
            $json = file_get_contents($path);
            $this->words = json_decode($json, true);
        }

        public function exist(string $word): bool
        {
            return array_search($word, $this->words) !== false;
        }
    }
?>