<?php
    require "rake.php";
    use RAKE\Rake;
    $str = file_get_contents(__DIR__ . '/../text.txt');
    $rake = new Rake($str);
    $keyWords = $rake->getKeyword();

    $result = '';
    foreach ($keyWords as $keyWord => $score) {
        $result .= $keyWord . ' ==> ' . $score . "\n\r";
    }
    echo $result;
?>